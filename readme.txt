git-checker
=========

Afin d'�viter d'installer des logiciels lourds pour visuellement voir vos repositories git ayant des fichiers non-commit�s, j'ai con�u ce petit utilitaire qui fera le tout pour vous.

Surtout utile pour les d�veloppeurs utilisant git en command-line, mais il est aussi efficace pour les gens sur SourceTree/Smartgit/etc..

## Usage

Retracer les repositories git contenant des modifications non commit�s � la fin d'une journ�e de travail, par exemple.

## Installation

|�LINUX - UNIX |
-- �videmment, vous devez avoir git d'install�

Lancez le script dans votre terminal, then enjoy !

| WINDOWS |
-- �videmment, vous devez avoir git d'install�

-- Ajouter le dossier des sources binaires de git dans votre "Environnement path"

1) Clique droit sur "Computer"
2) Cliquez sur "Advanced settings"
3) Ensuite "Environnement variables"
4) Et finalement "PATH"
5) Pour mon poste, par exemple, j'ai du ajouter ";C:\Program Files (x86)\Git\bin"

Ces operations vont vous permettre d'appeler la commande "git" par votre command prompt (cmd.exe)


Finalement, double-cliquez sur le .exe, vous devriez voir appara�tre les informations que vous recherchez :)

## Requirements

- Git
- library.zip

## Running the Tests

(� venir)

## Credits

Gabriel Robert

## License

git-checker is released under the MIT License.

The MIT License (MIT)
Copyright (c) 2013

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
