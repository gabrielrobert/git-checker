#!/usr/bin/env

'''
Git checker is used for personal purpose. All this shit can be done
with Tortoise or any other OS oriented services.
'''

import os
import sys
import subprocess as sub

import module_locator
import githelper

files = list()

def change_branch(path):
    ''' Create a new branch with the current modified files '''

    output, error = githelper.change_branch(path, 'autocommit', True)
    if error.__contains__('already exists.'):
        output, error = githelper.change_branch(path, 'autocommit', False)

    print output, error

def check_changed_files(path):
    '''
    Given a specific path, this method check if there is any
    changed uncommitted files
    '''

    command = sub.Popen(['git', 'status'], cwd=path, stdout=sub.PIPE, stderr=sub.PIPE)
    output, error = command.communicate()
    if output.__contains__('Changes not staged') and not error :
        files.append(path)


def expose_data():
    ''' Print the final messages to the user about his repositories '''

    for path in files:
        print '-----> ' + path
    if len(files) == 0:
        print "Tous vos fichiers ont ete commites."


def get_working_dir():
    ''' Get the current directory where the script is run '''

    return module_locator.module_path()


def search_git_dirs(args, dirname, filenames):
    ''' Search in a specific folder any .git folders '''


    for filename in filenames:
        absolute_path = os.path.join(dirname, filename)
        if os.path.isdir(absolute_path) and absolute_path.endswith(".git"):
            check_changed_files(absolute_path[:-4])


def start_process(depth):
    '''
    Recursively walk throught every child subfolders of the
    working directory
    '''

    # Root folder
    array = [get_working_dir()]

    for i in range(depth):

        # current array
        current_paths = array
        array = []

        for path in current_paths:
            for file in os.listdir(path):
                absolute_path = os.path.join(path, file)
                if os.path.isdir(absolute_path):
                    if file.endswith("git"):
                        check_changed_files(absolute_path[:-4])
                    else:
                        array.append(absolute_path)


def ask_for_commiting_or_exiting():
    ''' Do a serie of action to terminate the script '''

    print "\nTappez [ENTER] pour quitter ce script, ou [commit] pour lancer la serie de commit"
    return raw_input()


if __name__ == '__main__':
    print "Le dossier racine est actuellement: " + get_working_dir()

    # # searching in current directory for any git projects
    start_process(3)

    # show any non-commited files
    expose_data()

    if ask_for_commiting_or_exiting() == "commit":
        # User want to commit files in new branch
        githelper.add_current_modified_files(files[0])
        change_branch(files[0])
    else:
        # User want to exit the script
        print 'Fermeture du script'
