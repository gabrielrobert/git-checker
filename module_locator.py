''' Module used to get the path of the current running file '''

import sys, os

def we_are_frozen():
    ''' All of the modules are built-in to the interpreter '''

    return hasattr(sys, "frozen")

def module_path():
    ''' Used to determine Windows current path files '''

    encoding = sys.getfilesystemencoding()
    if we_are_frozen():
        return os.path.dirname(unicode(sys.executable, encoding))
    return os.path.dirname(unicode(__file__, encoding))
