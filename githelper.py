''' Util class used to execute git command '''

#!/usr/bin/env


import subprocess as sub

commit_message = "Fichiers auto-commites par git-checker."

def change_branch(path, branch_name, new_branch):
    ''' Create a new branch with the current modified files '''

    if new_branch:
        return execute_command(path, ['git',
                                      'checkout',
                                      '-b',
                                      'autocommit'])
    else:
        return execute_command(path, ['git',
                                      'checkout',
                                      'autocommit'])

def add_current_modified_files(path):
    ''' Add current modified files to the commit '''

    execute_command(path, ['git', 'add', '.'])

def commit_files(path):
    ''' Commit the current files '''

    execute_command(path, ['git', 'commit', '-am', commit_message])

def execute_command(path, commands):
    ''' Execute git commands '''

    command = sub.Popen(commands, cwd=path, stdout=sub.PIPE, stderr=sub.PIPE)
    return command.communicate()
